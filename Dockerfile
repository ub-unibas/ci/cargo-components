FROM scratch
ENTRYPOINT ["/app/app"]
WORKDIR /app
COPY target/app .
