//! A test application

fn main() {
    // Just for the sake of testing clippy component
    if !"".is_empty() {
        twice(3);
    } else {
        println!("Do nothing");
    }
}

/// Multiplies num by two
fn twice(num: usize) -> usize {
    num * 2
}

#[test]
fn test_twice() {
    assert_eq!(twice(2), 4);
}
