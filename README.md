# GitLab CI/CD Components for the cargo build tool

[CI/CD components](https://docs.gitlab.com/ee/ci/components/) are reusable
single pipeline configuration units. This repository contains such units for
testing and building cargo (rust) applications. They are used in projects maintained by the IT
department of the [University Library of Basel](https://ub.unibas.ch).

## Components

### test

Runs unit tests

#### Requirements

In case `test_report` is set to `true`, the `ci`-profile has to be defined in
the nextest configuration. At the least, the following lines have to be
present:

```toml
[profile.ci.junit]
path = "junit.xml"
```

For more information see https://nexte.st/docs/machine-readable/junit/

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `musl` | Build for `musl` target | `boolean` | `true` |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |
| `test_report` | Generate test report | `boolean` | `false` |

#### Artifact

Test report (`target/nextest/ci/junit.xml`) if `test_report` set to `true`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/test@main
```


### test-lint

Does code linting with [`clippy`](https://doc.rust-lang.org/stable/clippy). For
further information, see [lint categories](https://doc.rust-lang.org/stable/clippy) 
and [list of lints](https://doc.rust-lang.org/stable/clippy).

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `musl` | Build for `musl` target | `boolean` | `true` |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `allow_failure` | Only print warning if job fails | `boolean` | `false` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `correctness_level` | Level of clippy category `correctness` | `string` | `"deny"` |
| `suspicious_level` | Level of clippy category `suspicious` | `string` | `"warn"` |
| `style_level` | Level of clippy category `style` | `string` | `"warn"` |
| `complexity_level` | Level of clippy category `complexity` | `string` | `"warn"` |
| `perf_level` | Level of clippy category `perf` | `string` | `"warn"` |
| `pedantic_level` | Level of clippy category `pedantic` | `string` | `"allow"` |
| `nursery_level` | Level of clippy category `nursery` | `string` | `"allow"` |
| `cargo_level` | Level of clippy category `cargo` | `string` | `"allow"` | 
| `custom_levels` | Custom levels for clippy lints | `string` | `""` |
| `fail_on_warnings` | Deny clippy warnings and abort job with error | `boolean` | `true` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/test-lint@main
```


### test-audit

Runs an audit on packages

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `musl` | Build for `musl` target | `boolean` | `true` |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `allow_failure` | Only print warning if job fails | `boolean` | `false` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/test-audit@main
```


### test-coverage

Creates test coverage report. The report can be used to create a coverage badge, for instance.

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `musl` | Build for `musl` target | `boolean` | `true` |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Artifact

Coverage report (`coverage.xml`)

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/test-coverage@main
```


### test-miri

Runs miri for checking undefined behavior

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `allow_failure` | Only print warning if job fails | `boolean` | `false` |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/test-mir@main
```


### build

Creates a binary build

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `musl` | Build for `musl` target | `boolean` | `true` |
| `dev` | Development (non-optimised) build | `boolean` | `false` |
| `compress_binary` | Compress binary with upx | `boolean` | `true` |
| `stage` | Stage in CI/CD | `string` | `"build"` |
| `cargo_path` | Path to main `Cargo.toml` file | `string` | `"./Cargo.toml"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Artifact

- Binary (on `target/app`) with expiration date +1h

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/build@main
```


### SBOM

Creates a Software Bill of Materials (SBOM) with the [cargo-cyclonedx plugin](https://github.com/CycloneDX/cyclonedx-rust-cargo)

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"publish"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Artifact

SBOM file in `sbom/sbom.xml`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/sbom@main
```


### pages

Creates and publishes documentation as GitLab pages

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"deploy"` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `image_tag` | Tag of base image | `string` | `1.3.0` |

#### Artifact

Documentation in folder `public/`

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/pages@main
```


### cargo-default

Runs all components with default values and [publishes a Docker image](https://gitlab.switch.ch/ub-unibas.ch/ci/docker-components). Requires
stages `test`, `build` and `deploy` per default.

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `test_report` | Generate test report (see requirements in test section) | `boolean` | `false` |
| `internal_libs` | Depends on libraries published only on Switch GitLab | `boolean` | `false` |
| `allow_failure` | Only print warning if job fails | `boolean` | `false` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/cargo-components/cargo-default@main
```
